# Homework instructions

## Getting started

1. Under "hw-heroku-app" [Click here to get to the supgroup](https://gitlab.com/vse-data-x/devops-intro/hw-heroku-app). Click on `New Project` to create a new repository to host your app code 

2. Name the new project with your VSE username

3. Use the template provided [here](https://gitlab.com/vse-data-x/devops-intro/hw-heroku-app/pipeline-starter-code/-/blob/presley.design-main-patch-71726/.gitlab-ci.yml) to run your pipelines. Customize it to your needs and make sure to add the `tag: vse` to pick up a dedicated runner enabled for your project

4. The deliverable is an application in its own project created with your username along with its code and `.gitlab-ci.yml`file that has been executed succesfully and deployed to Heroku

5. Add a descriptive `README`to your project with the URL of the application deployed 

6. Remember to follow the steps described during the lecture and its recording
